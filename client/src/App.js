import React, {useState, useEffect} from 'react';
import './App.css';

function App() {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    fetch('/users')
      .then(res => res.json())
      .then(users => {setUsers(users); console.log(users)})
  }, [])

  return (
    <div className="App">
      {users.map(user => (
        <div key={user.id}>
          {user.username}
        </div>
      ))}
    </div>
  );
}

export default App;
